# GeneticNN #
An experiment in neural network generation using Genetic Algorithms.

## Files ##
- gen.py contains the classes for genetic algorithms from GenPy (https://bitbucket.org/jopro/genpy)
- nn.py contains the classes for basic neural networks (no learning)